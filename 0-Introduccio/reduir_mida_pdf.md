# HOWTO: com reduir la mida d'un pdf

> Problema: Volem reduir la mida d'un document pdf

*Solució: Farem servir *ghostcript*, concretament l'ordre `gs`*

```
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -sOutputFile=output.pdf input.pdf
```

Lògicament haureu de substituir `output.pdf` pel nom del vostre document pdf
reduit i `input.pdf` pel nom del vostre document pdf abans de reduir.

Les opcions de qualitat les dona el paràmetre `-dPDFSETTINGS` i els diferents
valors que pot tenir són:
 

```
-dPDFSETTINGS=/screen   (screen-view-only quality, 72 dpi images)
-dPDFSETTINGS=/ebook    (low quality, 150 dpi images)
-dPDFSETTINGS=/printer  (high quality, 300 dpi images)
-dPDFSETTINGS=/prepress (high quality, color preserving, 300 dpi imgs)
-dPDFSETTINGS=/default  (almost identical to /screen)
```



