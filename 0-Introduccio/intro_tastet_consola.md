# **_Introducció pràctica a la línia d'ordres_**


Es fa un "tastet" per introduir als alumnes al món de la consola.

Primer contacte amb la consola. Veiem primer com diferenciar el que és el directori "/home" del nostre directori "HOME". Per a aconseguir això, farem:

```
ls /
```
Veurem uns quants directoris, un d'ells és el _/home_:

Treballem ara amb l'ordre echo que fa un "eco!". Ens mostra, el que li escrivim com a argument, per exemple
```
echo "hola"
echo $HOSTNAME
echo $LANG
```

I finalment:
```
echo "$HOME"
```

Veiem que aquesta darrera ordre ens proporciona la trajectòria absoluta del nostre directori personal. En el _GNU/Linux_ de casa tindrem **/home/anna** si el meu compte es digués **anna**.

Un cop hem vist que no és el mateix el directori */home* que el *nostre home*(el qual es troba a la variable HOME) passem a jugar amb altres ordres.

Ordres a emprar:
```
man man
```
- Per sortir: tecla *q*.
- Per avançar o retrocedir -> deixem-nos guiar per l'intuició.
- Per cercar un patró o cadena de text: tecla */*. 
- Si l'anterior cerca troba més d'un ítem, per trobar la següent ocurrència: tecla *n* (tecla *N* per trobar l'anterior)




Una manera més gràfica de veure els directoris en forma d'arbre és amb l'ordre tree (mostra l'arbre en horitzontal en comptes de vertical):
```
tree /
```
Però clar, si executeu aquesta ordre ens mostra tot l'arbre del sistema i es fa interminable. 

#### **Exercici 1**:
Feu un man de l'ordre i esbrineu com mostrar en forma d'arbre els directoris des de /, però només fins al primer nivell i en color. 


#### **Exercici 2**:
Trobeu quina és l'opció del *ls* que ens mostra els fitxers ordenats per data de modificació. (Hint: *time*, *modification*)


Com em moc per l'arbre de directoris?
Amb l'ordre _change directory_:
```
cd
```

Com puc saber on em trobo?
Amb l'ordre _print working directory_
```
pwd
```

Juguem una mica executant les següents ordres:
```
cd /
pwd
cd /tmp
pwd
```
---

#### **Exercici 3**:
Com podria fer per anar ara al meu directori? De quantes maneres ho podem fer això?


#### **Solució**:
Al menys de 5 maneres totes utilitzant l'ordre *cd- amb l'argument:
- Alt Gr + 4 (Alt +126)
- $HOME
- trajectòria absoluta
- trajectòria relativa
- sense argument

---

Un parell de directoris *relatius* molt importants:

**..** directori pare

**.** directori actual


#### **PRÀCTICA**:
El directori a *gandhi* a on podem guardar fins a 100MB és */home/users/inf/jism1/ismwxxxxxxx/ismxxxxxx*. Volem crear una estructura de directoris amb els següents noms:
- M01_Equips_Informatics
- M02_Sistemes_Operatius
- M03_Ofimatica
- M04_Xarxes

a més del directori M01_Equips_Informatics penjarà un subdirectori practica1 amb la següent estructura:


![No Image? Wrong path then](tree_practice.png)


----

Heu de respondre a aquestes preguntes:

1. En quin directori us trobeu?
2. Quines són les ordres que heu emprat per crear aquesta estructura de directoris?
3. Hi ha algun paràmetre que permeti crear més d'un directori de cop?
4. Col·loqueu-vos al vostre home, editeu amb *vim* un fitxer anonomenat _fitxerA_ dintre del directori *subdirectori222* i deseu-lo.
5. Copieu-lo a *subdirectori12* (indiqueu a on us trobeu)
6. Canvieu el nom del *fitxerA* que es troba a *subdirectori222* per *fitxerB*
7. **Intenteu** eliminar el *subdirectori222* executant l'ordre:  ```
rm subdirectori222```
Que passa? No feu res, però digueu quina seria la solució?
8. Moveu el fitxer *fitxerB* que es troba a *subdirectori222* al *subdirectori12*.

Ordres que poden ajudar: **mkdir**, **cp**, **mv**, **rm** i com sempre **man** **_ordre_**.

Nota: El caràcter **~** es pot mostrar a la consola gnome-terminal, o terminator, amb una combinació de tecles especials i tenint en compte que el seu codi ASCII és el 126, que en base hexadecimal 126 s'expressa com a 7E: ```<Ctrl>+<Majusc.>+u7e``` també ens mostra aquell caràcter la combinació ```<Alt Gr> + 4```
Si volem trobar un caràcter molt curiós, pile of poo, seria ```<Ctrl>+<Majusc.> + u1f4a9``` i així obtenim ```💩``` 
