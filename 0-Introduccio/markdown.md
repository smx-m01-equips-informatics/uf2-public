# Markdown

## Definició 1

És un format de text simple per a l'escriptura de documents estructurats, basat en convencions de format de correu electrònic i Usenet (precursora dels fòrums d'internet).

## Definició 2

[Markdown](http://daringfireball.net/projects/markdown/) és un llenguatge de marques lleuger, originalment creat per John Gruber i Aaron Swartz que permet "escriure utilitzant un format de text planer fàcil d'escriure i de llegir i després convertir-ho en un XHTML o HTML estructuralment vàlid". 

## Importància de markdown

Per fer-nos una idea de l'importància d'aquest llenguatge podem fer un cop d'ull a:

- les diferents implementacions de markdown a la seva pàgina de la [Viquipèdia](https://en.wikipedia.org/wiki/Markdown).

- l'evolució de l'interés des de la seva creació al 2004 segons [Google Trends](https://www.google.com/trends/explore#q=markdown).


## Instal·lació d'eines de treball

Instal·lem un editor de codi, el conversor pandoc, i eines de correcció ortogràfica.

```
dnf -y install retext
dnf -y install pandoc texlive
dnf -y install aspell aspell-ca
```

Ara, ens centrarem en la sintaxi de markdown, però més endavant si ens interessa podrem jugar a convertir el document markdown en altres formats gràcies a [pandoc](http://pandoc.org/demos.html).

Se suposa que a la instal·lació de Fedora hem instal·lat chromium, altrament podem instal·lar-lo:
```
dnf install chromium
```
I afegir l'aplicació de stackedit.io per poder treballar offline.

Tanmateix, és interessant instal·lar algun plugin a firefox i/o a chromium per poder visualitzar documents d'aquest tipus. Per exemple per a chromium Markdown Plus.

D'altra banda si vulguéssim corregir l’ortografia d'un document fets amb Markdown, podem utilitzar una ordre com aquesta, si pensem que està escrit en català:

```
aspell --lang=ca check README.md
```


## Commonmark: una solució a la necessitat d'un estàndard

Quan John Gruber i Aaron Swartz van crear el llenguatge van definir unes
especificacions d'aquest llengutage que no va tenir en compte possibles
ambigüitats.

Arran d'aquesta *mancança* les diferents implementacions de markdown
divergeixen considerablement en els darrers 10 anys.  Com a resultat d'això els
usuaris es troben que un mateix document es comporta d'un manera a un sistema i
diferent a una altre.

De manera que es fa necessari establir un estàndard d'aquest llenguatge, i
efectivament, el mateix creador de pandoc lidera aquest estàndard de markdown:
[commonmark](http://commonmark.org/).

- [Especificacions](http://spec.commonmark.org/)
- [Mini xuleta](http://commonmark.org/help/)
- [Mini tutorial](http://commonmark.org/help/tutorial/)
- [Test](http://spec.commonmark.org/dingus/)



![meme markdown](markdown_do_you_speak_it.jpg)

## Alguns enllaços més:

[Xuleta de Markdown d'una pàgina](http://packetlife.net/media/library/16/Markdown.pdf)

[Xuleta de Markdown de dues pàgines](http://stationinthemetro.com/wp-content/uploads/2013/04/Markdown_Cheat_Sheet_v1-1.pdf)

[Un exemple de presentació feta amb markdown i pandoc](https://ia600205.us.archive.org/26/items/presentacions_projectes_EDT/slides/example16a2.html)

[El mateix exemple d'abans amb un altre estil](https://ia601509.us.archive.org/6/items/presentacions_projectes_EDT/slides/slides2.html)

