# Extract hardware info from host with linux

## System

+ System model
+ Bios version
+ Bios date
+ How old is the hardware?

## Mainboard

+ Mainboard model
+ Manual link
+ Product link

### Memory banks (free or occupied)

### Disks

+ How many disks and types can be connected?
+ How many disks and types are connected?

### Chipset

+ Chipset model
+ Chipset link

## CPU

+ cpu model
+ cpu year
+ cpu cores
+ cpu threads
+ cpu caches

### socket 

+ model

## PCI

### PCI slots

+ Number of pci slots
+ Availabble lanes
 
### Devices connected (to the PCI slots)

#### Network 

+ network device model
+ network kernel module (driver)
+ network device speed

#### Audio

+ audio device model
+ audio kernel model (driver)


#### Video (VGA)

+ video device model
+ video kernel module

