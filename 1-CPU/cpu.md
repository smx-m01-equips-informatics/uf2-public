## Característiques de la CPU

>_La CPU (unitat central de processament) o processador és el component de
l'ordinador que interpreta les instruccions contingudes en els programes i
processa les dades._

#### Situació actual

Anem a fer un cop d'ull a un problema d'actualitat: la mida dels processadors i
la seva relació amb la quantitat de transistors que conté.

[Sobre nanòmetres i CPUs](https://www.xataka.com/xataka/guerra-nanometros-hitos-barreras-que-se-estan-encontrando-fabricantes-procesadores-despeja-x-1x44)

[Àudio a archive.org](https://ia601402.us.archive.org/30/items/guerrananometroshitosbarreras-xataka/guerrananometroshitosbarreras-xataka.mp3)

#### Informació de les característiques de la CPU

Com puc trobar la informació sobre les diferents característiques d'una CPU determinada?

Hi ha diferents ordres que ens mostren una bona descripció de la nostra CPU, algunes de consola i d'altres gràfiques, algunes específiques de cpu i d'altres genèriques de maquinari (hardware) que poden informar-nos en particular de la CPU.

[En aquest enllaç](https://geekytheory.com/obtener-datos-de-la-cpu-en-linux-de-8-formas-diferentes) podeu trobar moltes eienes interessants, com per exemple:

* cat /proc/cpuinfo
* lscpu
* inxi
* lshw (i la versió gràfica lshw-gui)
* hardinfo










#### Instal·lacions extres

+ inxi

	```
	dnf install inxi
	```

+ lshw (i la versió gràfica lshw-gui)

	```
	dnf install lshw-gui  # això ja instal·la lshw
	```

+ hardinfo

	Ho farem amb repositori ja que així és més fàcil actualitazr automàticament l'eina.

	+ Assegurar-se que tenim instal·lat el repositori _rpmfusion_.
	+ Baixar-nos el darrer rpm rpmsphere-release [d'aqui](https://github.com/rpmsphere/noarch/blob/master/r/)
que per exemple ara mateix el podem baixar amb aquesta ordre:
	
		```
		wget https://github.com/rpmsphere/noarch/raw/master/r/rpmsphere-release-32-1.noarch.rpm
		```
	
	- Instal·lar el repositori:
	
		```
		rpm -Uvh rpmsphere-release*rpm
		```
	
	- Instal·lar el paquet hardinfo:
	
		``` 
		dnf install hardinfo
		```


#### LINKS EXTRES

- [Alguns conceptes interessants de la CPU](https://www.howtogeek.com/194756/cpu-basics-multiple-cpus-cores-and-hyper-threading-explained/)
