## Exercicis de representació de la informació

#### Exercici 1.
Quants Mbps són 40 Gbps?

#### Exercici 2.

La primera versió d'Ethernet de 1973 funcionava a 2,94 Mbps. Quants cops més
ràpida és l'Ethernet de 10Gbps respecte l'Ethernet original?

#### Exercici 3.

Tenim un flux de vídeo de 384 kbps, quants bytes per segon es transfereixen?

#### Exercici 4.

Les dades que van des d'una estació de treball d'usuari a un centre de xarxes
d'àrea d'emmagatzemament segueix la ruta que es mostra continuació:

+ Estació de treball – IDF	Ethernet de 10 Mbps
+ IDF – MDF			Ethernet ràpida de 100 Mbps
+ MDF – SAN			Ethernet Gigabit de 1000 Mbps

Quin és el millor temps de descàrrega estimat perquè aquest usuari descarregui
un arxiu de 50MB?

#### Exercici 5.

Suposem una tassa de transferència fixa de 100KBps. Quants MB ens hem baixat en
6 hores?

#### Exercici 6.

El host A permet una comunicació a 112 Kbps i el host B a 1,44 Mbps. El tràfic
de xarxa passa per un node que treballa a 28 Kbps. Calcula el temps de
descàrrega que correspondria en enviar de A a B el contingut d'un disquet de
1,44 MB?

#### Exercici 7.

Converteix els següents números binaris a base decimal:

1. 01101011
2. 10010110
3. 11101001
4. 00011011
5. 01111111
6. 1110

#### Exercici 8.

Converteix els següents números decimals a binaris:

1. 123
2. 202
3. 67
4. 7
5. 252
6. 91

#### Exercici 9.

Converteix els següents números hexadecimals a decimals:

1. A1B23
2. F34D
3. D21395F
4. 8634
5. A23E
6. FF

#### Exercici 10.

Converteix els següents números decimals a hexadecimals:
	
1. 123
2. 275954
3. 68745
4. 9
5. 15
6. 1599

#### Exercici 11.

Completa la següent taula:

Decimal  | Hexadecimal  | Binari
---------|--------------|-------
? | A9 | ?
? | FF | ?
? | BAD |?
? | FA:BA:DA | ?
? | A21C8 | ?
53 | ? | ? 
117 | ? | ? 
115 | ? | ? 
19 | ? | ? 
? | ? | 212.65.119.45 | ? | ? 
? | ? | 101010
? | ? | 1011101011
? | ? | 110
? | ? | 11111100.00111100
? | ? | 00001100.10000000.11110000.11111111


#### Exercici 12.

Quin bus és més ràpid, el d’una targeta de xarxa a 2.5 Gbps o el bus SATA2?
(Heu de cercar quina és la velocitat d’un bus SATA2) 

#### Exercici 13.

Vull fer una còpia de seguretat (backup) del meu disc dur de 500 GB que tinc
ocupat al 80%, quant trigaré a retransmetre a un disc dur extern per usb3 si el
disc dur [que teniu a aquest
enllaç](https://documents.westerndigital.com/content/dam/doc-library/en_us/assets/public/western-digital/product/data-center-drives/ultrastar-dc-hc600-series/data-sheet-ultrastar-dc-hc650.pdf)
funciona amb velocitats sostingudes de 238 MiB/s ?

#### Exercici 14.

Vull copiar un fitxer per la xarxa a 1Gbps de 23 GB, tenint en compte que els
protocols generen una sobrecàrrega (overhead) del 5% quant de temps trigaré a
fer la transmissió?
       
