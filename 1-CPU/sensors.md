## Eines de monitorització de la CPU (i més)

#### Eines que monitoritzen els programes

Recordem que la cpu s'encarrega d'executar les instruccions d'un programa.
Fem un cop d'ull a la utilització de la CPU quan s'estan executant diferents
tasques:

Podríem executar l'ordre `ps` amb algunes opcions interessants, per exemple `ps aux`, però això ens faria una foto en un instant determinat. Si volem un *vídeo*, en el sentit de que s'actualitza la informació cada cert interval de temps podem fer servir top:

```
top
```

El resultat podria ser semblant a aquest:

```
Tasks: 249 total,   1 running, 247 sleeping,   0 stopped,   1 zombie
%Cpu(s):  1.8 us,  1.4 sy,  0.0 ni, 93.9 id,  0.0 wa,  2.8 hi,  0.2 si,  0.0 st
MiB Mem :   7857.7 total,   2961.8 free,   2776.7 used,   2119.1 buff/cache
MiB Swap:   5120.0 total,   3121.4 free,   1998.6 used.   3818.1 avail Mem 

    PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND                                                                                
   1402 pingui    20   0 1103404 110368  62136 S   3.6   1.4  30:49.27 Xorg
   1591 pingui    20   0 4846496 164104  35464 S   2.6   2.0  24:57.53 gnome-shell
   1573 pingui     9 -11 3066880  11060   8824 S   2.0   0.1  30:00.66 pulseaudio
  26953 pingui    20   0  720032  51608  28756 S   1.7   0.6   0:49.19 terminator
  58976 pingui    20   0  228704   4416   3776 R   0.7   0.1   0:00.56 top
  ...
```

L'ordre top ens mostra una capçalera amb info interessant i els diferents
programes que s'estan executant, en principi per ordre de prioritat.

A la capçalera ens podem fixar a la línia del temps de la CPU (en %):

* us: percentatge d'ús de la CPU d'usuari (programes que està fent servir l'usuari)
* sy: percentatge d'ús de la CPU del sistema (el kernel del SO).
* id: percentatge d'ús de la CPU que està ociós, *idle*, sense fer res.
* wa: percentatge d'ús de la CPU que està esperant perquè el sistema està escrivint o llegint dades (waiting for IO acces)

I a sota es veuria cadascun dels programes que executa la CPU amb informació
relativa a aquestes: propietari, simpatia, % d'us de la CPU ....



L'eina `htop` és molt similar a `top` però té algunes característiques que top no té (scrolling, vista en forma d'arbre, actuar sobre un grup de programes al mateix temps.

```
dnf install htop 
```

Aquí htop té a dalt info dels cores de la CPU, de la memoria i de la swap.  A
sota els diferents programes als quals es poden enviar senyals veure els
subprocessos ...

Pot ser interessant fer servir l'ordre `stress` amb una cpu `stress -c 1 ` al
mateix temps que observem l'us dels cores de la CPU.

---

#### Les eines sensors i s-tui (freqüències, temperatures ...)

Ara també podem fer servir `stress -c 1` i a una altra secció de `terminator`
mirem com va la temperatura amb `sensors`. Això sí el problema de sensors és
que s'executa una vegada i prou. Podem fer servir l'ordre `watch` que executa
l'ordre que li posis a continuació indefinidament.

Provem amb un terminator i dues seccions executant:

```
stress -c 1
```

i a l'altra

```
watch sensors
```



Finalment podem instal·lar (un paquet de Python que s'instal·la com a usuari ordinari:

```
pip install s-tui --user
```

I ara podem executar s-tui per veure les frequències dels cores i les temperatures:

```
s-tui
```

Potser també estaria bé fer un stress amb una cpu.

---

#### Notes

* Si volem saber a quin paquet pertany un executable, per exemple `sensors`
  podem demanr-li a dnf qui em *proveeix* el paquet:

```
dnf provides sensors
```







