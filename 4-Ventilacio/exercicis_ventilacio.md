# Exercicis ventilació

Respon a aquestes preguntes en un document markdown.

### Exercici 1
Com ha d’anar el flux de l’aire per a una correcta ventilació?

### Exercici 2
Què indica l’etiqueta d’un ventilador?

### Exercici 3
3 formes que, normalment, indiquen la direcció de l’aire en un ventilador.

### Exercici 4
Quines parts són les que més s’escalfen en un ordinador?

### Exercici 5

Per a una bona ventilació, quina ha de ser més gran, l'entrada o la sortida d'aire? 

### Exercici 6

Quines conseqüències negatives pot tindre l’acumulació de pols?

### Exercici 7
En quina unitat es mesura la velocitat del ventilador?

### Exercici 8
Com reduir el soroll causat pel ventilador sense disminuir el flux de l’aire?

### Exercici 9
Llocs o components on pot haver un ventilador

### Exercici 10
De quin material estan fets els dissipadors?

### Exercici 11
De què depèn si els dissipadors porten ventilador o no
