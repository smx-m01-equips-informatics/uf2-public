# Ordres del bash per obtenir informació dels discs durs i els SSD



### Ordre `lsblk`

```
lsblk -O |less
```

```
lsblk -o MODEL,NAME,SIZE,MOUNTPOINT
```


```
[professor@localhost ~]$ lsblk 
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
loop0    7:0    0  97.1M  1 loop /var/lib/snapd/snap/core/9993
loop1    7:1    0  17.9M  1 loop /var/lib/snapd/snap/pdftk/9
sda      8:0    0 111.8G  0 disk 
├─sda1   8:1    0   105G  0 part /
└─sda2   8:2    0     5G  0 part [SWAP]
sdb      8:16   0 465.8G  0 disk 
├─sdb1   8:17   0     1K  0 part 
└─sdb5   8:21   0 465.8G  0 part 
sr0     11:0    1  1024M  0 rom 
```



```
[root@localhost ~]# alias lsdisk='lsblk -o NAME,MODEL,FSTYPE,SIZE,TYPE,MOUNTPOINT'
[root@localhost ~]# lsdisk 
NAME   MODEL                     FSTYPE     SIZE TYPE MOUNTPOINT
loop0                            squashfs  97.1M loop /var/lib/snapd/snap/core/9993
loop1                            squashfs  17.9M loop /var/lib/snapd/snap/pdftk/9
sda    Samsung_SSD_850_EVO_120GB          111.8G disk 
├─sda1                           ext4       105G part /
└─sda2                           swap         5G part [SWAP]
sdb    TOSHIBA_DT01ACA050                 465.8G disk 
├─sdb1                                        1K part 
└─sdb5                           ext4     465.8G part 
sr0    HL-DT-ST_DVDRAM_GH24NSC0            1024M rom  
```






### Ordre `smartctl`

Una de les ordres més interessants per a informació de discs durs és
`smartctl`. Per esbrinar a quin paquet es troba aquest executable podem fer
servir dnf (no cal ser root ja que volem info, per instal·lar després sí que ho
necessitem ):

```
dnf provides smartctl
...
smartmontools-1:7.1-8.fc32.x86_64 : Tools for monitoring SMART capable hard disks
...
```

Instal·lem:

```
dnf install smartctl 
```

Com sempre 

```
smartctl --help
```

Una molt bona opció és:

```
smartctl --all /dev/sdb | less
```

Aquesta opció `--all` és molt completa. En funció del dispositiu inclou més
opcions, però les opcions que inclou en tots els casos són `-H -i -c -a -l
error` que mostren característiques del dispositiu d'emmagatzematge excepte
`-H` (`--health`) que mostra l'estat de *salut* del dispositiu.


### Altres ordres

Recordem l'ordre `lshw` que mostra informació de tot el maquinari.

```
lshw -class disk -sanitize
```

L'opció `-sanitize` ens amaga informació sensible com podria ser el *serial
number*.



#### Instruccions per obrir port i permetre accès a un systemrescuecd per ssh

Definim la política d'acceptar tot el que entra i surt per la xarxa:

```
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
```

Flush i esborrat de regles:

```
iptables -F
iptables -X
```

Posem un password a root:

```
passwd
```

Canviem la distribució del teclat:

```
loadkey es
```
o
```
setkmap es
```


Amb el rescue cd podem:

Arrancar interfície gràfica:
```
startx
```

Compartir terminal amb altres:

+ obrint terminal compartida
	```
	tmux
	```

+ entrar a una terminal compartida (*attach session*)
	```
	tmux a
	```


Molt bones opcions extres en aquest link:
https://www.cyberciti.biz/faq/find-hard-disk-hardware-specs-on-linux/

smartctl:
https://www.thomas-krenn.com/en/wiki/SMART_tests_with_smartctl
