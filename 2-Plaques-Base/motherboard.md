# La placa mare (motherboard)


> La **placa mare**, **placa base** o **targeta mare** (**motherboard**) és la [targeta de circuits impresos](https://ca.wikipedia.org/wiki/Circuit_imprès) central a l'ordinador que conté el microprocessador, la memòria RAM del sistema, circuits electrònics de suport, la ROM i ranures especials (*slots*) que permeten la connexió de targetes adaptadores addicionals.


## Factor de forma

*Un ordinador personal es compon de diverses peces independents entre si, la placa mare, la caixa, la font d'alimentació, etc. Cadascun d'aquests components és proporcionat per un fabricant independent. Si no hi ha un acord mínim entre aquests fabricants, podríem tenir problemes com per exemple que una placa base no pogués entrar físicament en una carcassa, o que l'endoll d'una font d'alimentació pogués ser incompatible amb el corresponent connector de la placa base ...de manera que necessitem un estàndard, uns acords mínims*

Aquest *acord de mínims* és el factor de forma. Consisteix en definir unes característiques molt bàsiques d'una placa base com:

+ les dimensions físiques exactes: ample i llarg.
+ la posició dels ancoratges: coordenades on se situen els cargols.
+ les àrees on se situen les ranures d'expansió, i els connectors de la part posterior (teclat, ratolí, USB, etc).
+ la forma física del connector de la font d'alimentació.
+ les connexions elèctriques de la font d'alimentació (cables, voltatges necessaris ...)

 
Algunes mides de refència:

+ ATX (305×244)=> format *tower* de caixa
+ microATX (244×244) => hi ha menys espai per tarjes d'expansió (*mini tower* )
+ mini-ITX => per models molt petits (per exemple caixes d'ordinador per multimedia)
+ Extended ATX => per servidors o plaques més grans
+ Altres per servidors en rack

![Imatge amb factors de forma de diferents mides](img/mainboard_1603783062.png)

## Elements de la placa mare

*Anem a detallar de manera una mica més específica alguns dels components de la placa base*

### Processador (CPU)

Ja vist al tema anterior

### Sòcol de la CPU

> El sòcol de la CPU (CPU socket) és una peça de suport de la CPU per a on es
> connecta aquesta a la placa base.
 
**L'elecció del socket és molt important a una placa mare, ja que condiciona la
família de processadors compatibles.**

En efecte, un conjunt de processadors d'una mateixa arquitectura i generació comparteixen un encapsulat compatible. Per exemple un processador intel [i5-7500](https://ark.intel.com/content/www/us/en/ark/products/97123/intel-core-i5-7500-processor-6m-cache-up-to-3-80-ghz.html) i un intel [i7-7700](https://ark.intel.com/content/www/us/en/ark/products/97128/intel-core-i7-7700-processor-8m-cache-up-to-4-20-ghz.html) pertanyen a la 7a generació de processadors d'Intel, amb un encapsulat igual que es correspon amb un socket FCLGA1151. A una mateixa placa base amb aquest tipus de socket li podem "punxar" qualsevol d'aquests dos processadors. 

A continuació una imatge d'un i5, per la cara superior on anirà el dissipador (element físic destinat a eliminar l'excès de calor produit per un component de *hardware*), per la cara inferior (la que connecta cada pin amb el socket de la placa mare) i el socket:

![imatge de la cara superior d'una cpu i5](img/i5-7500_front.png) ![imatge de la cara inferior d'una cpu i5](img/i5-7500_pins.png)

![imatge del socket compatible](img/socket_1151.png)


*Sockets habituals a Novembre de 2020*:

+ Intel 1151 (rev1, rev2, rev3): des de 2015 fins 2020 diferents arquitectures de processadors Intel
+ Intel 2066 (Skylake-X): Processadors "gama alta" i9, i7... 
+ Socket AM4: AMD Ryzen 3,5,7,9
+ AMD TR4: AMD Ryzen™ Threadripper™ (alta gama AMD)
+ AMD sTRX4: AMD Ryzen™ Threadripper™ (2a generació de l'anterior)

*CPUs compatibles amb el socket*:

És habitual que el fabricant de la placa base tingui una llista de cpus
compatibles i, en alguns casos, **cal fer un update de la bios** (canviar el
firmware) per tal d'admetre aquestes cpus.

Per exemple, per una placa mare [Gigabyte B450M DS3H](https://www.gigabyte.com/Motherboard/B450M-DS3H-rev-10/support#support-cpu) tenim un llistat de cpus compatibles:

![](img/mainboard_1603783695.png)


### Refrigeració

Els components de la placa base generen calor i això pot espatallar el funcionament dels mateixos o d'altres components, per tant es fa necessari un sistema de refrigeració. Hi ha moltes maneres de refrigerar, algunes de les més importants serien:

+ Ventiladors (FAN)
+ Disseny (layout) de la placa base
+ Refrigeració líquida 


### Joc de Xips (chipset)

És el conjunt de processadors disenyat per treballar conjuntament per controlar
els components de la placa base i els perifèrics externs.

Inicialment tenia dos elements el *northbridge* i el *southbridge*. El primer feia de
pont entre els elements més ràpids (RAM, GPU i components connectats a PCIe) i
la CPU, mentre que el segon s'encarregava dels més *lents* (controladora de disc
IDE, SATA, PCI, ethernet, ports USB ...)

Northbridge i southbridge s'unien mitjançant el DMI (direct media interface
d'Intel).

![diagrama del joc de xips_antic](img/chipset_diagram.svg)

En les darreres generacions de CPU el northbridge [s'integra directament amb la
CPU](https://hardzone.es/2018/12/02/integrado-north-bridge-procesador/), de
manera que les versions 2 i 3 del DMI ja es diu que enllaça la CPU amb el
chipset (southbridge).

![diagrama del joc de xips_nou](img/kaby-lake-s-desktop-block-diagram.png)

---

Per exemple, pel chipset z490 de intel tenim aquestes [especificacions](https://www.intel.es/content/www/es/es/products/chipsets/desktop-chipsets/z490.html) que ens dona el fabricant del chipset que és Intel. Tenim 6 ports SATA que pot governar el chipset.

![](img/mainboard_1603785663.png)

---

I tenim per dos plaques mare ( [Z490 AORUS XTREME WATERFORCE](https://www.gigabyte.com/Motherboard/Z490-AORUS-XTREME-WATERFORCE-rev-1x) i [Z490I AORUS ULTRA](https://www.gigabyte.com/Motherboard/Z490I-AORUS-ULTRA-rev-1x)  ) amb el mateix chipset amb diferentes nombres de ports (6x sata i 4xsata):

![](img/mainboard_1603785516.png)


### Memòria

_Aclariment: quan es parla de memòria (a seques) ens referim a la RAM_

La memòria RAM és la memòria principal d'un dispositiu a on s'emmagatzemen
programes i dades, també és coneix com a memòria volàtil, ja que quan no
s'alimenta per cap font d'energia (o sigui està *apagada*) aquesta informació
es perd.


A les especificacions de la placa base ens trobem un llistat de memòries
compatibles. Però pot haver més d'un llistat si la placa mare és compatible amb
més d'una família de cpus. Per exemple per una placa amb socket AMD: B550 AORUS
PRO AX a la web de suport tenim dos llistats per AMD Matisse o AMD Renoir:

![imatge de les llistes de suport de la memòria](img/mainboard_1603784476.png)

Els llistats són semblants a:

![](img/mainboard_1603784608.png)

### Ranures d'expansió (_slots_): PCIe

Una ranura d'expansió, _slot_, és un element de la placa base que permet
connectar a aquesta una targeta addicional.   

Si parlem de les plaques mare actuals, parlarem exclusivament de PCIe, però si
mirem una placa mare antiga trobarem els seus predecessors:

+ ISA
+ PCI  
+ AGP (per les GPUs)

PCIe és un estàndard (una especificació) de bus informàtic que permet connectar
targetes d'expansió i comunicar aquestes targetes amb la CPU.

El bus PCIe està estructurat com enllaços [punt a
punt](https://ca.wikipedia.org/wiki/Punt_a_punt), *full duplex*, treballant en
sèrie. La teoria, o sigui l'estàndard, diu que cada slot d'expansió porta
**1**, 2, **4**, **8**, **16** o 32 lanes de dades entre la placa base i la
targeta connectada, en negreta els lanes que a la pràctica existeixen.

Un processador de gama alta tindrà més *lanes* de PCIe que un de gama més baixa.

Hi ha 4 versions del bus PCIe:

+ Versió 1.x: 2.5 [GT/s](https://en.wikipedia.org/wiki/GT/s) (hi ha compatibilitat amb aquestes versions més velles)
  - x1: 2 Gbps
  - x16: 32 Gbps
+ Versió 2.x: 5 GT/s (hi ha compatibilitat amb aquestes versions més velles)
  - **x1:** 4 Gbps
  - **x16:** 64 Gbps
+ **Versió 3.x: 8 GT/s** (és el més habitual)
  - **x1: 7.88 Gbps**
  - **x16: 126.08 Gbps**
+ **Version 4.0:16 GT/s** (de moment a nov-2020 disponible amb processadors AMD)
  - **x1: 15.76 Gbps**
  - **x16: 252.16 Gbps**

*Tipus de dispositius que es connecten al bus PCIe*:

+ Targeta gràfica: algunes en comptes d'estar integrades al processador són
  dedicades. En aquest cas, com que és el dispositiu més exigent a nivell de
velocitat, utilitza una amplada de bus de **16 lanes**.

+ Targeta de xarxa: l'amplada del bus dependrà de de la velocitat de xarxa.
	- Targetes de xarxa de 10 Gbps

		D'una banda he de suportar fluxos de dades de transmissió i
recepció, recordem que era full duplex, que vol dir que es pot enviar i rebre a la
vegada, per tant necessito 2 * 10 Gbps = 20 Gbps.

		D'altra banda si faig servir PCIe v3 necessito 4 lanes (x4),
pensem que no existeix x3:

			x1: 7.88 Gbps  (insuficient: no arriba als 20 Gbps)
			x2: 15.76 Gbps (insuficient: no arriba als 20 Gbps)
			x4: 31.52 Gbps <---- aquest sí

		![imatge targeta de xarxa PCIe](img/mainboard_1603786436.png)

		I si fes servir PCIe v2? (Exercici)

	- Targetes de xarxa de 1 Gbps

		Fent servir un raonament semblant, necessito transferències de
2 * 1 Gbps = 2 Gbps, amb v3 i una lane x1 seria suficient.		
 
		![imatge targeta de xarxa](img/mainboard_1603787350.png)

+ Targeta de so: l'àudio consumeix molt poc ample de banda i és habitual que
  funcionin amb 1 lane PCI

![](img/mainboard_1603787909.png)

+ Unitat **NVME** (dispositiu d'enmagatzematge molt ràpid). Per cada dispositiu NVME es necessiten 4 lanes de PCIe. Tenen habitualment un connector diferent que es el **M.2** que envia 4 lanes PCIe v3 . Ja hem calculat abans la velocitat màxima, que serà el límit d'aquest disc NVME: 31.52 GBps.

	![](img/mainboard_1603788378.png)

	- Per exemple per un disc nvme ***Samsung 970 EVO Plus 500GB SSD NVMe M.2*** tenim:
		
		- Velocitat de lectura: 28 Gbps
		- Velocidad d'escriptura: 25.6 Gbps

	- Hi ha targetes que permeten fer una conversió de connectors de PCIe a M.2

	![](img/mainboard_1603788491.png)

+ Controladores de disc per connectar molts discos ssd o rotacionals en raid.

### Busos d'emmagatzematge

#### Bus IDE / SCSI

No és la tecnologia actual, però encara hi ha ordinadors antics que en tenen. Les dades es transmeten en paral·lel i no permet velocitats elevades.
![](img/mainboard_1603884760.png)

#### Bus SATA (Serial ATA)

Bus serie amb menys cables, transmet més velocitat de dades. Mida reduida que permet integrar a la placa mare molts connectors.

Amb la tecnologia SSD (Solid state disk) es podian aconseguir velocitats molt elevades. Van treure diferents estàndars per aconseguir aquesta velocitat:

+ **SATA**: 1.2 Gbps
+ **SATA 2**: 2.4 Gbps
+ **SATA 3**: 4.8 Gbps

Si el bus SATA és més ràpid pot arribar a saturar el bus DMI i per això van
haver de canviar de tecnologia del BUS.

Les conversions de bus SATA a PCI generen latència (retards).

El connector SATA tradicional és:

![](img/mainboard_1603885046.png)

M.2 per SATA que es diferencia del M.2 NVME per la forma:

![](img/mainboard_1603887854.png)

### Àudio

Chips o controladora d'àudio que es comunica amb el chipset

Els connectors d'audio:

+ Rear panel 

![](img/mainboard_1603888851.png)

+ Front pannel

![](img/mainboard_1603888766.png)

### USB

USB (universal serial bus) pensat per connectar qualsevol tipus de dispositiu.

Connectors:

![](img/mainboard_1603889021.png)



Versions:

+ USB 1.1
+ USB 2 
+ USB 3.0
+ USB 3.1
+ USB 3.2 : 20 Gbit/s
                       
### Port Serie (COM)

Segueix els estàndars de port sèrie més antics. Es pot connectar externament
amb un adaptador. Si hi han diferents ports serie es numeren com COM1, COM2 ...

![](../img/mainboard_1603889720.png)

## LINKS:

+ [Joc de xips (chipset)](http://www3.gobiernodecanarias.org/medusa/ecoblog/jmaspio/2012/04/30/7/)


