# Pràctica de placa base


#### Creació d'un resum de manual d'una placa base

Heu d'escollir una placa base, que no sigui de server.

El document pdf el creareu a partir de la següent informació:

+ Una primera pàgina que contindrà:
	- Una foto del producte
	- URL del producte
	- URL del manual
	- Versió 1a i darrera versió de la BIOS
	- URL's d'alguns preus
+ Especificacions del manual de la placa base
+ Disseny (layout) de la placa base (manual)
+ Disseny de blocs de la placa base (manual)

Com fareu el pdf?

- La primera pàgina la fareu amb el writer del libreoffice i després exportareu a pdf.
- Com que el manual estarà en format pdf fareu servir l'ordre `qpdf` que potser no teniu instal·lada per seleccionar pàgines de pdocuments pdf's i concaternar-les per obtenir un document final pdf.

Exemple d'ús:

```
qpdf --empty --pages path_to_file1.pdf 1,6-8 path_to_file2.pdf 3,4,5 -- path_output.pdf

```


[Exemple fet per l'alumna Scarlett Johansson](resum_mainboard_scarlet.pdf)


