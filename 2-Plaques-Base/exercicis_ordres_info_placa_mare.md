# Exercicis placa base


#### Exercici 1

De les diferents ordres que disposem a GNU/Linux quina ordre ens mostra informació de la placa base a partir de la taula DMI (SMBIOS)?

#### Exercici 2

Es necessita ser root per fer servir l'ordre de l'exercici 1?

#### Exercici 3

Amb l'ordre de l'exercici 1 mostra informació general de la placa base.

#### Exercici 4

Amb l'ordre de l'exercici 1 mostra informació dels connectors general de la placa base.

#### Exercici 5

Volem connectar el nostre portàtila a la pantalla de la tele, per això volem
confirmar el connector de video que tenim, després de fer servir l'ordre de
l'exercici anterior veiem que la sortida ens mostra:

```
Handle 0x0004, DMI type 8, 9 bytes
Port Connector Information
	Internal Reference Designator: JMDP
	Internal Connector Type: None
	External Reference Designator: MiniDP
	External Connector Type: Other
	Port Type: Video Port
```

Mostra una imatge d'un cable que es pugui connectar al nostre portàtil per aquest port (tant és el que hi hagi a l'altra banda del cable).

#### Exercici 6

Mostra la versió de la BIOS amb l'ordre que estem fent servir.

#### Exercici 7

Amb l'ordre que estàs treballant, mostra el tipus o descripció del sistema de refrigeració

#### Exercici 8

Hi ha alguna manera de fer servir l'ordre de l'exercici 1 sense ser root? Fes un cop d'ull al man i mostra la versió de la BIOS sense fer servir l'ordre anterior i sense ser root.

#### Exercici 9

Quina ordre ens mostra informació dels busos PCI i dels dispositius connectats
a aquests busos?


#### Exercici 10

Amb l'ordre anterior vull mostrar tots els drivers dels diferents dispositius pci


#### Exercici 11

Amb quina ordre i opcions he aconseguit aquesta sortida per pantalla?

```
Slot:   00:00.0
Class:  Host bridge [0600]
Vendor: Intel Corporation [8086]
Device: Xeon E3-1200 v2/3rd Gen Core processor DRAM Controller [0150]
SVendor:        Gigabyte Technology Co., Ltd [1458]
SDevice:        Device [5000]
Rev:    09

Slot:   00:02.0
Class:  VGA compatible controller [0300]
Vendor: Intel Corporation [8086]
Device: Xeon E3-1200 v2/3rd Gen Core processor Graphics Controller [0152]
SVendor:        Gigabyte Technology Co., Ltd [1458]
SDevice:        Device [d000]
Rev:    09
...
```

#### Exercici 12

Després d'esbrinar quina ordre i opcions mostra aquesta sortida,

```
...
00:1f.3 0c05: 8086:1c22 (rev 05)
00:1f.5 0101: 8086:1c08 (rev 05)
02:00.0 0c03: 1b6f:7023 (rev 01)
03:00.0 0200: 10ec:8168 (rev 06)
```

vull obtenir informació per internet del darrer dispositiu pci que veig a la
sortida, com faria la cerca?


